btnTheme = document.querySelector(".btn-change-color")
btnTheme.addEventListener('click', () => {
   btnTheme.dataset.theme === "light" ? btnTheme.dataset.theme = "dark"
      : btnTheme.dataset.theme = "light";
   localStorage.setItem('theme', btnTheme.dataset.theme);
   changeTheme(btnTheme.dataset.theme);
})

function changeTheme(themeName) {
   let themeUrl = `./css/${themeName}-theme.css`
   document.querySelector('[title="theme"]').setAttribute('href', themeUrl);
}

let activeTheme = localStorage.getItem('theme');
if (activeTheme !== null) {
   changeTheme(activeTheme);
   btnTheme.dataset.theme = activeTheme;
}